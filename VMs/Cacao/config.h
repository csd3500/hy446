/* config.h.  Generated from config.h.in by configure.  */
/* config.h.in.  Generated from configure.ac by autoheader.  */

/* Define if building universal (internal helper macro) */
/* #undef AC_APPLE_UNIVERSAL_BUILD */

/* architecture directory */
#define ARCH_DIR "x86_64"

/* Old ARM architecture without good Thumb interworking */
/* #undef ARM_NO_THUMB_IW */

/* Java core library classes at compile time */
#define BUILD_JAVA_RUNTIME_LIBRARY_CLASSES "/usr/local/classpath/share/classpath/glibj.zip"

/* CACAO's Mercurial revision */
#define CACAO_HGREV ""

/* library installation prefix */
#define CACAO_LIBDIR "/usr/local/lib"

/* installation prefix */
#define CACAO_PREFIX "/usr/local"

/* CACAO's vm.zip */
#define CACAO_VM_ZIP "/usr/local/share/cacao/vm.zip"

/* disable dump memory */
/* #undef DISABLE_DUMP */

/* disable garbage collector */
/* #undef DISABLE_GC */

/* enable annotations */
#define ENABLE_ANNOTATIONS 1

/* enable assertion */
#define ENABLE_ASSERTION 1

/* enable cycle count statistics */
/* #undef ENABLE_CYCLES_STATS */

/* debug filter */
#define ENABLE_DEBUG_FILTER 1

/* enable disassembler */
/* #undef ENABLE_DISASSEMBLER */

/* Enable dynamic library loading. */
#define ENABLE_DL 1

/* enable escape analysis with ssa */
/* #undef ENABLE_ESCAPE */

/* enable generating code to validate escape analysis results */
/* #undef ENABLE_ESCAPE_CHECK */

/* enable conservative boehm-gc */
#define ENABLE_GC_BOEHM 1

/* enable exact cacao-gc */
/* #undef ENABLE_GC_CACAO */

/* enable handles (indirection cells) */
/* #undef ENABLE_HANDLES */

/* enable if-conversion */
#define ENABLE_IFCONV 1

/* use method inlining */
/* #undef ENABLE_INLINING */

/* enable method inlining debug options */
/* #undef ENABLE_INLINING_DEBUG */

/* enable interpreter */
/* #undef ENABLE_INTRP */

/* compile for Java ME CLDC1.1 */
/* #undef ENABLE_JAVAME_CLDC1_1 */

/* compile for Java SE */
#define ENABLE_JAVASE 1

/* enable JIT compiler */
#define ENABLE_JIT 1

/* enable JNI */
#define ENABLE_JNI 1

/* enable JRE layout */
#define ENABLE_JRE_LAYOUT 1

/* use JVMTI */
/* #undef ENABLE_JVMTI */

/* enable libjvm.so */
#define ENABLE_LIBJVM 1

/* enable logging */
/* #undef ENABLE_LOGGING */

/* use loop optimization */
/* #undef ENABLE_LOOP */

/* enable lsra */
/* #undef ENABLE_LSRA */

/* perform debugging memory checks */
/* #undef ENABLE_MEMCHECK */

/* use opagent */
/* #undef ENABLE_OPAGENT */

/* enable profiling */
/* #undef ENABLE_PROFILING */

/* enabled python pass */
/* #undef ENABLE_PYTHON */

/* use on-stack replacement */
/* #undef ENABLE_REPLACEMENT */

/* enable real-time timing */
/* #undef ENABLE_RT_TIMING */

/* enable softfloat */
/* #undef ENABLE_SOFTFLOAT */

/* enable soft double cmp */
/* #undef ENABLE_SOFT_DOUBLE_CMP */

/* enable soft float cmp */
/* #undef ENABLE_SOFT_FLOAT_CMP */

/* enable lsra with ssa */
/* #undef ENABLE_SSA */

/* link CACAO statically */
/* #undef ENABLE_STATICVM */

/* enable statistics */
/* #undef ENABLE_STATISTICS */

/* enable threads */
#define ENABLE_THREADS 1

/* enable thread local heap */
/* #undef ENABLE_TLH */

/* enable classfile verification */
#define ENABLE_VERIFIER 1

/* enable vmlog tracing code */
/* #undef ENABLE_VMLOG */

/* use zlib */
#define ENABLE_ZLIB 1

/* Define to 1 if you have the `abort' function. */
#define HAVE_ABORT 1

/* Define to 1 if you have the `accept' function. */
#define HAVE_ACCEPT 1

/* Define to 1 if you have the `access' function. */
#define HAVE_ACCESS 1

/* Define to 1 if you have the <assert.h> header file. */
#define HAVE_ASSERT_H 1

/* Define to 1 if you have the `atoi' function. */
#define HAVE_ATOI 1

/* Define to 1 if you have the `atol' function. */
#define HAVE_ATOL 1

/* Define to 1 if you have the <avcall.h> header file. */
/* #undef HAVE_AVCALL_H */

/* Define to 1 if you have the `backtrace' function. */
#define HAVE_BACKTRACE 1

/* Define to 1 if you have the `backtrace_symbols' function. */
#define HAVE_BACKTRACE_SYMBOLS 1

/* Define to 1 if you have the `calloc' function. */
#define HAVE_CALLOC 1

/* Define to 1 if you have the `close' function. */
#define HAVE_CLOSE 1

/* Define to 1 if you have the `confstr' function. */
#define HAVE_CONFSTR 1

/* Define to 1 if you have the `connect' function. */
#define HAVE_CONNECT 1

/* Define to 1 if you have the <dirent.h> header file, and it defines `DIR'.
   */
#define HAVE_DIRENT_H 1

/* Define to 1 if you have the `dirname' function. */
#define HAVE_DIRNAME 1

/* Define to 1 if you have the `dlclose' function. */
#define HAVE_DLCLOSE 1

/* Define to 1 if you have the `dlerror' function. */
#define HAVE_DLERROR 1

/* Define to 1 if you have the <dlfcn.h> header file. */
#define HAVE_DLFCN_H 1

/* Define to 1 if you have the `dlopen' function. */
#define HAVE_DLOPEN 1

/* Define to 1 if you have the `dlsym' function. */
#define HAVE_DLSYM 1

/* Define to 1 if you have the <errno.h> header file. */
#define HAVE_ERRNO_H 1

/* Define to 1 if you have the <execinfo.h> header file. */
#define HAVE_EXECINFO_H 1

/* Define to 1 if you have the `fclose' function. */
#define HAVE_FCLOSE 1

/* Define to 1 if you have the <fcntl.h> header file. */
#define HAVE_FCNTL_H 1

/* Define to 1 if you have the <ffi.h> header file. */
/* #undef HAVE_FFI_H */

/* Define to 1 if you have the `fflush' function. */
#define HAVE_FFLUSH 1

/* Define to 1 if you have the `fopen' function. */
#define HAVE_FOPEN 1

/* Define to 1 if you have the `fprintf' function. */
#define HAVE_FPRINTF 1

/* Define to 1 if you have the `fread' function. */
#define HAVE_FREAD 1

/* Define to 1 if you have the `free' function. */
#define HAVE_FREE 1

/* Define to 1 if you have the `fstat' function. */
#define HAVE_FSTAT 1

/* Define to 1 if you have the `fsync' function. */
#define HAVE_FSYNC 1

/* Define to 1 if you have the `ftruncate' function. */
#define HAVE_FTRUNCATE 1

/* Define to 1 if you have the `getcwd' function. */
#define HAVE_GETCWD 1

/* Define to 1 if you have the `getenv' function. */
#define HAVE_GETENV 1

/* Define to 1 if you have the `gethostbyname' function. */
#define HAVE_GETHOSTBYNAME 1

/* Define to 1 if you have the `gethostname' function. */
#define HAVE_GETHOSTNAME 1

/* Define to 1 if you have the `getloadavg' function. */
#define HAVE_GETLOADAVG 1

/* Define to 1 if you have the `getpagesize' function. */
#define HAVE_GETPAGESIZE 1

/* Define to 1 if you have the `getpid' function. */
#define HAVE_GETPID 1

/* Define to 1 if you have the `getrusage' function. */
#define HAVE_GETRUSAGE 1

/* Define to 1 if you have the `getsockname' function. */
#define HAVE_GETSOCKNAME 1

/* Define to 1 if you have the `getsockopt' function. */
#define HAVE_GETSOCKOPT 1

/* Define to 1 if you have the `gettimeofday' function. */
#define HAVE_GETTIMEOFDAY 1

/* Define to 1 if you have the `htons' function. */
#define HAVE_HTONS 1

/* Define to 1 if you have the <inttypes.h> header file. */
#define HAVE_INTTYPES_H 1

/* Define to 1 if you have the `ioctl' function. */
#define HAVE_IOCTL 1

/* Define to 1 if you have the `isnan' function. */
#define HAVE_ISNAN 1

/* Define if your <locale.h> file defines LC_MESSAGES. */
#define HAVE_LC_MESSAGES 1

/* Define to 1 if you have the `avcall' library (-lavcall). */
/* #undef HAVE_LIBAVCALL */

/* Define to 1 if you have the `bfd' library (-lbfd). */
/* #undef HAVE_LIBBFD */

/* Define to 1 if you have the `dl' library (-ldl). */
#define HAVE_LIBDL 1

/* Define to 1 if you have the `ffi' library (-lffi). */
/* #undef HAVE_LIBFFI */

/* Define to 1 if you have the <libgen.h> header file. */
#define HAVE_LIBGEN_H 1

/* Define to 1 if you have the `iberty' library (-liberty). */
/* #undef HAVE_LIBIBERTY */

/* Define to 1 if you have the `intl' library (-lintl). */
/* #undef HAVE_LIBINTL */

/* Define to 1 if you have the `opagent' library (-lopagent). */
/* #undef HAVE_LIBOPAGENT */

/* Define to 1 if you have the `opcodes' library (-lopcodes). */
/* #undef HAVE_LIBOPCODES */

/* Define to 1 if you have the `pthread' library (-lpthread). */
#define HAVE_LIBPTHREAD 1

/* Define to 1 if you have the `rt' library (-lrt). */
/* #undef HAVE_LIBRT */

/* Define to 1 if you have the `socket' library (-lsocket). */
/* #undef HAVE_LIBSOCKET */

/* Define to 1 if you have the `z' library (-lz). */
#define HAVE_LIBZ 1

/* Define to 1 if you have the `listen' function. */
#define HAVE_LISTEN 1

/* Define to 1 if you have the <locale.h> header file. */
#define HAVE_LOCALE_H 1

/* Define to 1 if you have the `localtime' function. */
#define HAVE_LOCALTIME 1

/* Define to 1 if you have the `localtime_r' function. */
#define HAVE_LOCALTIME_R 1

/* Define to 1 if you have the `lseek' function. */
#define HAVE_LSEEK 1

/* Define to 1 if you have the <mach/mach.h> header file. */
/* #undef HAVE_MACH_MACH_H */

/* Define to 1 if you have the `malloc' function. */
#define HAVE_MALLOC 1

/* Define to 1 if you have the `memcpy' function. */
#define HAVE_MEMCPY 1

/* Define to 1 if you have the <memory.h> header file. */
#define HAVE_MEMORY_H 1

/* Define to 1 if you have the `memset' function. */
#define HAVE_MEMSET 1

/* Define to 1 if you have the `mmap' function. */
#define HAVE_MMAP 1

/* Define to 1 if you have the `mprotect' function. */
#define HAVE_MPROTECT 1

/* Define to 1 if you have the <ndir.h> header file, and it defines `DIR'. */
/* #undef HAVE_NDIR_H */

/* Define to 1 if you have the <netdb.h> header file. */
#define HAVE_NETDB_H 1

/* Define to 1 if you have the <opagent.h> header file. */
/* #undef HAVE_OPAGENT_H */

/* Define to 1 if you have the `open' function. */
#define HAVE_OPEN 1

/* Define to 1 if you have the `printf' function. */
#define HAVE_PRINTF 1

/* Define to 1 if you have the `read' function. */
#define HAVE_READ 1

/* Define to 1 if you have the `readlink' function. */
#define HAVE_READLINK 1

/* Define to 1 if you have the `realloc' function. */
#define HAVE_REALLOC 1

/* Define to 1 if you have the `recv' function. */
#define HAVE_RECV 1

/* Define to 1 if you have the <regex.h> header file. */
#define HAVE_REGEX_H 1

/* Define to 1 if you have the `scandir' function. */
#define HAVE_SCANDIR 1

/* Define to 1 if you have the `select' function. */
#define HAVE_SELECT 1

/* Define to 1 if you have the `send' function. */
#define HAVE_SEND 1

/* Define to 1 if you have the `setlocale' function. */
#define HAVE_SETLOCALE 1

/* Define to 1 if you have the `setsockopt' function. */
#define HAVE_SETSOCKOPT 1

/* Define to 1 if you have the `shutdown' function. */
#define HAVE_SHUTDOWN 1

/* Define to 1 if you have the <signal.h> header file. */
#define HAVE_SIGNAL_H 1

/* Define to 1 if you have the `socket' function. */
#define HAVE_SOCKET 1

/* Define to 1 if you have the `stat' function. */
#define HAVE_STAT 1

/* Define to 1 if you have the <stdarg.h> header file. */
#define HAVE_STDARG_H 1

/* Define to 1 if you have the <stdbool.h> header file. */
#define HAVE_STDBOOL_H 1

/* Define to 1 if you have the <stdint.h> header file. */
#define HAVE_STDINT_H 1

/* Define to 1 if you have the <stdio.h> header file. */
#define HAVE_STDIO_H 1

/* Define to 1 if you have the <stdlib.h> header file. */
#define HAVE_STDLIB_H 1

/* Define to 1 if you have the `str2sig' function. */
/* #undef HAVE_STR2SIG */

/* Define to 1 if you have the `strcat' function. */
#define HAVE_STRCAT 1

/* Define to 1 if you have the `strchr' function. */
#define HAVE_STRCHR 1

/* Define to 1 if you have the `strcmp' function. */
#define HAVE_STRCMP 1

/* Define to 1 if you have the `strcpy' function. */
#define HAVE_STRCPY 1

/* Define to 1 if you have the `strdup' function. */
#define HAVE_STRDUP 1

/* Define to 1 if you have the `strerror' function. */
#define HAVE_STRERROR 1

/* Define to 1 if you have the <strings.h> header file. */
#define HAVE_STRINGS_H 1

/* Define to 1 if you have the <string.h> header file. */
#define HAVE_STRING_H 1

/* Define to 1 if you have the `strlen' function. */
#define HAVE_STRLEN 1

/* Define to 1 if you have the `strncmp' function. */
#define HAVE_STRNCMP 1

/* Define to 1 if you have the `strstr' function. */
#define HAVE_STRSTR 1

/* Define to 1 if you have the <sys/dir.h> header file, and it defines `DIR'.
   */
/* #undef HAVE_SYS_DIR_H */

/* Define to 1 if you have the <sys/ioctl.h> header file. */
#define HAVE_SYS_IOCTL_H 1

/* Define to 1 if you have the <sys/loadavg.h> header file. */
/* #undef HAVE_SYS_LOADAVG_H */

/* Define to 1 if you have the <sys/mman.h> header file. */
#define HAVE_SYS_MMAN_H 1

/* Define to 1 if you have the <sys/ndir.h> header file, and it defines `DIR'.
   */
/* #undef HAVE_SYS_NDIR_H */

/* Define to 1 if you have the <sys/param.h> header file. */
#define HAVE_SYS_PARAM_H 1

/* Define to 1 if you have the <sys/resource.h> header file. */
#define HAVE_SYS_RESOURCE_H 1

/* Define to 1 if you have the <sys/select.h> header file. */
#define HAVE_SYS_SELECT_H 1

/* Define to 1 if you have the <sys/socket.h> header file. */
#define HAVE_SYS_SOCKET_H 1

/* Define to 1 if you have the <sys/stat.h> header file. */
#define HAVE_SYS_STAT_H 1

/* Define to 1 if you have the <sys/time.h> header file. */
#define HAVE_SYS_TIME_H 1

/* Define to 1 if you have the <sys/types.h> header file. */
#define HAVE_SYS_TYPES_H 1

/* Define to 1 if you have the <sys/utsname.h> header file. */
#define HAVE_SYS_UTSNAME_H 1

/* Define to 1 if you have the `time' function. */
#define HAVE_TIME 1

/* Define to 1 if you have the <time.h> header file. */
#define HAVE_TIME_H 1

/* Define to 1 if you have the <ucontext.h> header file. */
#define HAVE_UCONTEXT_H 1

/* Define to 1 if you have the <unistd.h> header file. */
#define HAVE_UNISTD_H 1

/* Define to 1 if you have the `vfprintf' function. */
#define HAVE_VFPRINTF 1

/* Define to 1 if you have the `write' function. */
#define HAVE_WRITE 1

/* Define to 1 if you have the <zconf.h> header file. */
#define HAVE_ZCONF_H 1

/* Define to 1 if you have the <zlib.h> header file. */
#define HAVE_ZLIB_H 1

/* have __thread */
#define HAVE___THREAD 1

/* Java core library hpi.h header */
/* #undef INCLUDE_HPI_H */

/* Java core library hpi_md.h header */
/* #undef INCLUDE_HPI_MD_H */

/* Java runtime library jmm.h header */
/* #undef INCLUDE_JMM_H */

/* Java runtime library jni.h header */
#define INCLUDE_JNI_H "/usr/local/classpath/include/jni.h"

/* Java runtime library jni_md.h header */
#define INCLUDE_JNI_MD_H "/usr/local/classpath/include/jni_md.h"

/* Java runtime library jvmti.h header */
/* #undef INCLUDE_JVMTI_H */

/* Java runtime library jvm.h header */
/* #undef INCLUDE_JVM_H */

/* Java runtime library jvm_md.h header */
/* #undef INCLUDE_JVM_MD_H */

/* Java architecture name */
#define JAVA_ARCH "amd64"

/* Java runtime library classes */
#define JAVA_RUNTIME_LIBRARY_CLASSES "/usr/local/classpath/share/classpath/glibj.zip"

/* Java runtime library native libraries installation directory */
/* #undef JAVA_RUNTIME_LIBRARY_LIBDIR */

/* Java runtime library installation directory */
#define JAVA_RUNTIME_LIBRARY_PREFIX "/usr/local/classpath"

/* Define to the sub-directory in which libtool stores uninstalled libraries.
   */
#define LT_OBJDIR ".libs/"

/* disable debug code */
/* #undef NDEBUG */

/* Name of package */
#define PACKAGE "cacao"

/* Define to the address where bug reports for this package should be sent. */
#define PACKAGE_BUGREPORT "cacao@cacaojvm.org"

/* Define to the full name of this package. */
#define PACKAGE_NAME "cacao"

/* Define to the full name and version of this package. */
#define PACKAGE_STRING "cacao 1.6.1"

/* Define to the one symbol short name of this package. */
#define PACKAGE_TARNAME "cacao"

/* Define to the home page for this package. */
#define PACKAGE_URL ""

/* Define to the version of this package. */
#define PACKAGE_VERSION "1.6.1"

/* Define as the return type of signal handlers (`int' or `void'). */
#define RETSIGTYPE void

/* The size of `void *', as computed by sizeof. */
#define SIZEOF_VOID_P 8

/* statement for skipping 16 bytes */
/* #undef SKIP16 */

/* Define to 1 if you have the ANSI C header files. */
#define STDC_HEADERS 1

/* Define to 1 if you can safely include both <sys/time.h> and <time.h>. */
#define TIME_WITH_SYS_TIME 1

/* Define to 1 if your <sys/time.h> declares `struct tm'. */
/* #undef TM_IN_SYS_TIME */

/* use scheduler */
/* #undef USE_SCHEDULER */

/* Version number of package */
#define VERSION "1.6.1"

/* CC used */
#define VERSION_CC "gcc"

/* CFLAGS used */
#define VERSION_CFLAGS "-g -O2 -fno-strict-aliasing  -std=c99 -pedantic -Wall -Wno-long-long   -I/usr/local/classpath/include -I/usr/local/classpath/include"

/* configure arguments */
#define VERSION_CONFIGURE_ARGS " '--prefix=/usr/local' '--with-java-runtime-library-prefix=/usr/local/classpath' '--enable-jre-layout' --enable-boehm-threads=posix"

/* CXX used */
#define VERSION_CXX "g++"

/* CXXFLAGS used */
#define VERSION_CXXFLAGS "-g -O2 -fno-strict-aliasing  -std=c++98 -pedantic -Wall -Wno-long-long   -I/usr/local/classpath/include -I/usr/local/classpath/include"

/* extra version info */
#define VERSION_EXTRA ""

/* full version info */
#define VERSION_FULL "1.6.1"

/* major version number */
#define VERSION_MAJOR 1

/* micro version number */
#define VERSION_MICRO 1

/* minor version number */
#define VERSION_MINOR 6

/* use binutils disassembler */
/* #undef WITH_BINUTILS_DISASSEMBLER */

/* use libffcall */
/* #undef WITH_FFCALL */

/* use libffi */
/* #undef WITH_FFI */

/* use Sun's CLDC1.1 classes */
/* #undef WITH_JAVA_RUNTIME_LIBRARY_CLDC1_1 */

/* use GNU Classpath */
#define WITH_JAVA_RUNTIME_LIBRARY_GNU_CLASSPATH 1

/* use OpenJDK's Java SE classes */
/* #undef WITH_JAVA_RUNTIME_LIBRARY_OPENJDK */

/* use OpenJDK's version 7 */
/* #undef WITH_JAVA_RUNTIME_LIBRARY_OPENJDK_7 */

/* Define WORDS_BIGENDIAN to 1 if your processor stores words with the most
   significant byte first (like Motorola and SPARC, unlike Intel). */
#if defined AC_APPLE_UNIVERSAL_BUILD
# if defined __BIG_ENDIAN__
#  define WORDS_BIGENDIAN 1
# endif
#else
# ifndef WORDS_BIGENDIAN
/* #  undef WORDS_BIGENDIAN */
# endif
#endif

/* Define to `__attribute__' to nothing if it's not supported.  */
#ifndef __cplusplus
/* #undef __attribute__ */
#endif

/* Define to empty if `const' does not conform to ANSI C. */
/* #undef const */

/* Define to `__inline__' or `__inline' if that's what the C compiler
   calls it, or to nothing if 'inline' is not supported under any name.  */
#ifndef __cplusplus
/* #undef inline */
#endif

/* Define to `long int' if <sys/types.h> does not define. */
/* #undef off_t */

/* Define to `unsigned int' if <sys/types.h> does not define. */
/* #undef size_t */
