
const unsigned heap_default_initial_size = 50*1024*1024;
const unsigned heap_default_maximum_size = 100*1024*1024;
const char *rvm_version = "Jikes RVM 3.1.4+git (revision 85a66b36e55f919948aca901862b1ddb36c188b4)";
const char *rvm_configuration = "prototype-opt";
const char *rvm_host_configuration = "/home/goku/Downloads/jikesrvm/build/configs/prototype-opt.properties";
const char *rvm_target_configuration = "/home/goku/Downloads/jikesrvm/build/targets/x86_64-linux.properties";
