public class SimpleLoop {

	public static void main(String args[]) {

		long start,stop;
		int i,n;

		start = System.nanoTime();

		i = 1;
		n = 1000000000;

		while(i < n){
			i = 2*i;
		}

	        stop = System.nanoTime();

		System.out.println(stop - start);

	}

}
