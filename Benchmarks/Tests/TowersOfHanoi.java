public class TowersOfHanoi {

	public void solve(int n, String start, String auxiliary, String end) {

		if (n != 1) {
			solve(n - 1, start, end, auxiliary);
			solve(n - 1, auxiliary, start, end);
		}

	}

	public static void main(String[] args) {

		TowersOfHanoi towersOfHanoi;
		long start,stop;
		int discs;

		start = System.nanoTime();

		towersOfHanoi = new TowersOfHanoi();
		discs = 22;

		towersOfHanoi.solve(discs, "A", "B", "C");

		stop = System.nanoTime();

		System.out.println(stop - start);

	}

}
