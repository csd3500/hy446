public class LoopCall {

	private static int mul(int a, int b) {

		return (a*b);

	}

	public static void main(String[] args) {

		long start,stop;
		int i,j,n;

	        start = System.nanoTime();

		i = 2;
		j = 2;
		n = 1000000;

		while(i+j < n){
			i = mul(i,j);
			j = mul(i,j);
		}

		stop = System.nanoTime();

		System.out.println(stop - start);

	}


}
