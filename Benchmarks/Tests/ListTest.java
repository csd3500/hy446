import java.util.*;

public class ListTest {

	public static void main (String[] args) {

		long start,stop;
		List l1;
		List l2;
		int i;

		start = System.nanoTime();

		l1 = new ArrayList();
		l2 = new ArrayList();

		for (i=0;i<10000;i++) {
			l1.add(i);
		}

		for (i=0;i<10000;i++) {
			l1.remove(l1.size()- 1);
			l2.add(i);
		}

		for (i=0;i<10000;i++) {
			l2.remove(l2.size() - 1);
			l1.add(i);
		}

		for (i=0;i<10000;i++) {
			l1.remove(l1.size() - 1);
		}

		stop = System.nanoTime();

		System.out.println(stop - start);

	}
}
