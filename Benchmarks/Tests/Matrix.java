import java.util.*;

public class Matrix {

	public static int[][] MatrixMul(int[][] A, int[][] B) {

		int i,j,k;

		int C [][] =
			{

				{0, 0, 0},
				{0, 0, 0},
				{0, 0, 0}
			};

		for(i=0;i<3;i++) {
			for(j=0;j<3;j++) {
				for(k=0;k<3;k++) {
					C[i][j] += A[i][k] * B[k][j];
				}
			}
		}

		return C;

	}

	public static void main(String[] args) {

		long start,stop;
		int n,i;

		start = System.nanoTime();

		i = 1;
		n = 1000000;

		int A [][] =
			{
				{2,  4,  6 },
				{8,  10, 12},
				{14, 16, 18}
			};

		int B [][] =
			{
				{-2, -4,  -6},
				{-6, -10, -12},
				{-14,-16, -18}
			};

		int C [][] =
			{
				{0, 0, 0},
				{0, 0, 0},
				{0, 0, 0}
			};

		while(i<n){
			C = MatrixMul(A,B);
			i++;
		}

		stop = System.nanoTime();

		System.out.println(stop - start);


	}


}
