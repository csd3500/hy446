#!/bin/bash
# Basic while loop
counter=1
j=0
i=0
while [ $counter -le 10000 ]
do
    j="$(java SimpleLoop 2>&1)"
    ((i=i+j))
   ((counter++))
done
((i=i/10000))
echo SimpleLoop:
echo $i

counter=1
j=0
i=0
while [ $counter -le 10000 ]
do
    j="$(java LoopCall 2>&1)"
    ((i=i+j))
   ((counter++))
done
((i=i/10000))
echo LoopCall:
echo $i

counter=1
j=0
i=0
while [ $counter -le 1000 ]
do
    j="$(java ListTest 2>&1)"
    ((i=i+j))
   ((counter++))
done
((i=i/1000))
echo ListTest:
echo $i

counter=1
j=0
i=0
while [ $counter -le 1000 ]
do
    j="$(java Matrix 2>&1)"
    ((i=i+j))
   ((counter++))
done
((i=i/1000))
echo Matrix:
echo $i

counter=1
j=0
i=0
while [ $counter -le 1000 ]
do
    j="$(java TowersOfHanoi 2>&1)"
    ((i=i+j))
   ((counter++))
done
((i=i/1000))
echo TowersOfHanoi:
echo $i

counter=1
j=0
i=0
while [ $counter -le 1000 ]
do
    j="$(java BinaryTree 2>&1)"
    ((i=i+j))
   ((counter++))
done
((i=i/1000))
echo BinaryTree:
echo $i
