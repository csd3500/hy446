
%SimpleLoop

Hotspot17(1:1347) = 1;
Hotspot18(1:1049) = 2;
Maxine(1:517)   = 3;
Jikes(1:470)    = 4;
Cacao(1:7165)   = 5;

ALL = horzcat(Hotspot17, Hotspot18, Maxine, Jikes, Cacao);

C = categorical(ALL, [1 2 3 4 5], {'Hotspot 1.7', 'Hotspot 1.8', 'Maxine', 'Jikes', 'Cacao'});

figure(1);
hold on;
title('Simple Loop: n = 1b 10k average (ns)');
histogram(C, 'BarWidth', 0.5, 'FaceColor', 'r');
hold off;

Hotspot17 = zeros(1,length(Hotspot17));
Hotspot18 = zeros(1,length(Hotspot18));
Maxine  = zeros(1,length(Maxine));
Jikes   = zeros(1,length(Jikes));
Cacao   = zeros(1,length(Cacao));

%LoopCall

Hotspot17(1:4116) = 1;
Hotspot18(1:2712) = 2;
Maxine(1:25713) = 3;
Jikes(1:86845)  = 4;
Cacao(1:20146)  = 5;

ALL = horzcat(Hotspot17, Hotspot18, Maxine, Jikes, Cacao);

C = categorical(ALL, [1 2 3 4 5], {'Hotspot 1.7', 'Hotspot 1.8', 'Maxine', 'Jikes', 'Cacao'});

figure(2);
hold on;
title('Loop Call: n = 1m 10k average (ns)');
histogram(C, 'BarWidth', 0.5, 'FaceColor', 'r');
hold off;

Hotspot17 = zeros(1,length(Hotspot17));
Hotspot18 = zeros(1,length(Hotspot18));
Maxine  = zeros(1,length(Maxine));
Jikes   = zeros(1,length(Jikes));
Cacao   = zeros(1,length(Cacao));

%TowersOfHanoi

Hotspot17(1:10071) = 1;
Hotspot18(1:19742) = 2;
Maxine(1:8965)   = 3;
Jikes(1:43523)   = 4;
Cacao(1:17227)   = 5;

ALL = horzcat(Hotspot17, Hotspot18, Maxine, Jikes, Cacao);

C = categorical(ALL, [1 2 3 4 5], {'Hotspot 1.7', 'Hotspot 1.8', 'Maxine', 'Jikes', 'Cacao'});

figure(3);
hold on;
title('Towers Of Hanoi: n = 22 disks 1k average (\mus)');
histogram(C, 'BarWidth', 0.5, 'FaceColor', 'r');
hold off;

Hotspot17 = zeros(1,length(Hotspot17));
Hotspot18 = zeros(1,length(Hotspot18));
Maxine  = zeros(1,length(Maxine));
Jikes   = zeros(1,length(Jikes));
Cacao   = zeros(1,length(Cacao));

%Matrix

Hotspot17(1:71435) = 1;
Hotspot18(1:76726) = 2;
Maxine(1:199201) = 3;
Jikes(1:2350552) = 4;
Cacao(1:967665)  = 5;

ALL = horzcat(Hotspot17, Hotspot18, Maxine, Jikes, Cacao);

C = categorical(ALL, [1 2 3 4 5], {'Hotspot 1.7', 'Hotspot 1.8', 'Maxine', 'Jikes', 'Cacao'});

figure(4);
hold on;
title('Matrix: n = 1m 1k average (\mus)');
histogram(C, 'BarWidth', 0.5, 'FaceColor', 'r');
hold off;

Hotspot17 = zeros(1,length(Hotspot17));
Hotspot18 = zeros(1,length(Hotspot18));
Maxine  = zeros(1,length(Maxine));
Jikes   = zeros(1,length(Jikes));
Cacao   = zeros(1,length(Cacao));

%BinaryTree

Hotspot17(1:834228) = 1;
Hotspot18(1:78748)  = 2;
Maxine(1:1197620) = 3;
Jikes(1:834910)   = 4;
Cacao(1:718406)   = 5;

ALL = horzcat(Hotspot17, Hotspot18, Maxine, Jikes, Cacao);

C = categorical(ALL, [1 2 3 4 5], {'Hotspot 1.7', 'Hotspot 1.8', 'Maxine', 'Jikes', 'Cacao'});

figure(5);
hold on;
title('Binary Tree: (Jikes Stack Overflow) 1k average (\mus)');
histogram(C, 'BarWidth', 0.5, 'FaceColor', 'r');
hold off;

Hotspot17 = zeros(1,length(Hotspot17));
Hotspot18 = zeros(1,length(Hotspot18));
Maxine  = zeros(1,length(Maxine));
Jikes   = zeros(1,length(Jikes));
Cacao   = zeros(1,length(Cacao));

%ListTest

Hotspot17(1:8255)  = 1;
Hotspot18(1:19742) = 2;
Maxine(1:5201)   = 3;
Jikes(1:23696)   = 4;
Cacao(1:7658)    = 5;

ALL = horzcat(Hotspot17, Hotspot18, Maxine, Jikes, Cacao);

C = categorical(ALL, [1 2 3 4 5], {'Hotspot 1.7', 'Hotspot 1.8', 'Maxine', 'Jikes', 'Cacao'});

figure(6);
hold on;
title('List Test: (using ArrayList) 1k average (\mus)');
histogram(C, 'BarWidth', 0.5, 'FaceColor', 'r');
hold off;

%antlr

Hotspot17 = zeros(1,length(Hotspot17));
Hotspot18 = zeros(1,length(Hotspot18));
Maxine  = zeros(1,length(Maxine));
Jikes   = zeros(1,length(Jikes));
Cacao   = zeros(1,length(Cacao));

Hotspot17(1:4938) = 1;
Hotspot18(1:4407) = 2;
Maxine(1:7029)  = 3;
Jikes(1:12717)  = 4;
Cacao(1:3671)   = 5;

ALL = horzcat(Hotspot17, Hotspot18, Maxine, Jikes, Cacao);

C = categorical(ALL, [1 2 3 4 5], {'Hotspot 1.7', 'Hotspot 1.8', 'Maxine', 'Jikes', 'Cacao'});

figure(7);
hold on;
title('antlr: (ms)');
histogram(C, 'BarWidth', 0.5, 'FaceColor', 'r');
hold off;

%bloat

Hotspot17 = zeros(1,length(Hotspot17));
Hotspot18 = zeros(1,length(Hotspot18));
Maxine  = zeros(1,length(Maxine));
Jikes   = zeros(1,length(Jikes));
Cacao   = zeros(1,length(Cacao));

Hotspot17(1:5759) = 1;
Hotspot18(1:6498) = 2;
Maxine(1:14454) = 3;
Jikes(1:37755)  = 4;
Cacao(1:16571)  = 5;

ALL = horzcat(Hotspot17, Hotspot18, Maxine, Jikes, Cacao);

C = categorical(ALL, [1 2 3 4 5], {'Hotspot 1.7', 'Hotspot 1.8', 'Maxine', 'Jikes', 'Cacao'});

figure(8);
hold on;
title('bloat: (ms)');
histogram(C, 'BarWidth', 0.5, 'FaceColor', 'r');
hold off;

%eclipse

Hotspot17 = zeros(1,length(Hotspot17));
Hotspot18 = zeros(1,length(Hotspot18));
Maxine  = zeros(1,length(Maxine));
Jikes   = zeros(1,length(Jikes));
Cacao   = zeros(1,length(Cacao));

Hotspot17(1:1)     = 1;
Hotspot18(1:44546) = 2;
Maxine(1:47608)  = 3;
Jikes(1:166084)  = 4;
Cacao(1:54124)   = 5;

ALL = horzcat(Hotspot17, Hotspot18, Maxine, Jikes, Cacao);

C = categorical(ALL, [1 2 3 4 5], {'Hotspot 1.7', 'Hotspot 1.8', 'Maxine', 'Jikes', 'Cacao'});

figure(9);
hold on;
title('eclipse (Hotspot error): (ms)');
histogram(C, 'BarWidth', 0.5, 'FaceColor', 'r');
hold off;

%fop

Hotspot17 = zeros(1,length(Hotspot17));
Hotspot18 = zeros(1,length(Hotspot18));
Maxine  = zeros(1,length(Maxine));
Jikes   = zeros(1,length(Jikes));
Cacao   = zeros(1,length(Cacao));

Hotspot17(1:3397) = 1;
Hotspot18(1:3199) = 2;
Maxine(1:4127)  = 3;
Jikes(1:9549)   = 4;
Cacao(1:2712)   = 5;

ALL = horzcat(Hotspot17, Hotspot18, Maxine, Jikes, Cacao);

C = categorical(ALL, [1 2 3 4 5], {'Hotspot 1.7', 'Hotspot 1.8', 'Maxine', 'Jikes', 'Cacao'});

figure(10);
hold on;
title('fop: (ms)');
histogram(C, 'BarWidth', 0.5, 'FaceColor', 'r');
hold off;

%hsqldb

Hotspot17 = zeros(1,length(Hotspot17));
Hotspot18 = zeros(1,length(Hotspot18));
Maxine  = zeros(1,length(Maxine));
Jikes   = zeros(1,length(Jikes));
Cacao   = zeros(1,length(Cacao));

Hotspot17(1:7720) = 1;
Hotspot18(1:8138) = 2;
Maxine(1:5854)  = 3;
Jikes(1:63916)  = 4;
Cacao(1:1)      = 5;

ALL = horzcat(Hotspot17, Hotspot18, Maxine, Jikes, Cacao);

C = categorical(ALL, [1 2 3 4 5], {'Hotspot 1.7', 'Hotspot 1.8', 'Maxine', 'Jikes', 'Cacao'});

figure(11);
hold on;
title('hsqldb (Cacao out of memory): (ms)');
histogram(C, 'BarWidth', 0.5, 'FaceColor', 'r');
hold off;

%jython

Hotspot17 = zeros(1,length(Hotspot17));
Hotspot18 = zeros(1,length(Hotspot18));
Maxine  = zeros(1,length(Maxine));
Jikes   = zeros(1,length(Jikes));
Cacao   = zeros(1,length(Cacao));

Hotspot17(1:9397) = 1;
Hotspot18(1:8573) = 2;
Maxine(1:13706) = 3;
Jikes(1:30319)  = 4;
Cacao(1:16675)  = 5;

ALL = horzcat(Hotspot17, Hotspot18, Maxine, Jikes, Cacao);

C = categorical(ALL, [1 2 3 4 5], {'Hotspot 1.7', 'Hotspot 1.8', 'Maxine', 'Jikes', 'Cacao'});

figure(12);
hold on;
title('jython: (ms)');
histogram(C, 'BarWidth', 0.5, 'FaceColor', 'r');
hold off;

%luindex

Hotspot17 = zeros(1,length(Hotspot17));
Hotspot18 = zeros(1,length(Hotspot18));
Maxine  = zeros(1,length(Maxine));
Jikes   = zeros(1,length(Jikes));
Cacao   = zeros(1,length(Cacao));

Hotspot17(1:8538) = 1;
Hotspot18(1:7436) = 2;
Maxine(1:7677)  = 3;
Jikes(1:25459)  = 4;
Cacao(1:17070)  = 5;

ALL = horzcat(Hotspot17, Hotspot18, Maxine, Jikes, Cacao);

C = categorical(ALL, [1 2 3 4 5], {'Hotspot 1.7', 'Hotspot 1.8', 'Maxine', 'Jikes', 'Cacao'});

figure(13);
hold on;
title('luindex: (ms)');
histogram(C, 'BarWidth', 0.5, 'FaceColor', 'r');
hold off;

%lusearch

Hotspot17 = zeros(1,length(Hotspot17));
Hotspot18 = zeros(1,length(Hotspot18));
Maxine  = zeros(1,length(Maxine));
Jikes   = zeros(1,length(Jikes));
Cacao   = zeros(1,length(Cacao));

Hotspot17(1:5707) = 1;
Hotspot18(1:7923) = 2;
Maxine(1:11870)  = 3;
Jikes(1:34974)  = 4;
Cacao(1:19713)  = 5;

ALL = horzcat(Hotspot17, Hotspot18, Maxine, Jikes, Cacao);

C = categorical(ALL, [1 2 3 4 5], {'Hotspot 1.7', 'Hotspot 1.8', 'Maxine', 'Jikes', 'Cacao'});

figure(14);
hold on;
title('lusearch: (ms)');
histogram(C, 'BarWidth', 0.5, 'FaceColor', 'r');
hold off;

%pmd

Hotspot17 = zeros(1,length(Hotspot17));
Hotspot18 = zeros(1,length(Hotspot18));
Maxine  = zeros(1,length(Maxine));
Jikes   = zeros(1,length(Jikes));
Cacao   = zeros(1,length(Cacao));

Hotspot17(1:5621) = 1;
Hotspot18(1:6521) = 2;
Maxine(1:10440) = 3;
Jikes(1:30975)  = 4;
Cacao(1:12806)  = 5;

ALL = horzcat(Hotspot17, Hotspot18, Maxine, Jikes, Cacao);

C = categorical(ALL, [1 2 3 4 5], {'Hotspot 1.7', 'Hotspot 1.8', 'Maxine', 'Jikes', 'Cacao'});

figure(15);
hold on;
title('pmd: (ms)');
histogram(C, 'BarWidth', 0.5, 'FaceColor', 'r');
hold off;


%xalan

Hotspot17 = zeros(1,length(Hotspot17));
Hotspot18 = zeros(1,length(Hotspot18));
Maxine  = zeros(1,length(Maxine));
Jikes   = zeros(1,length(Jikes));
Cacao   = zeros(1,length(Cacao));

Hotspot17(1:6927)  = 1;
Hotspot18(1:10311) = 2;
Maxine(1:8709)   = 3;
Jikes(1:38179)   = 4;
Cacao(1:24151)   = 5;

ALL = horzcat(Hotspot17, Hotspot18, Maxine, Jikes, Cacao);

C = categorical(ALL, [1 2 3 4 5], {'Hotspot 1.7', 'Hotspot 1.8', 'Maxine', 'Jikes', 'Cacao'});

figure(16);
hold on;
title('xalan: (ms)');
histogram(C, 'BarWidth', 0.5, 'FaceColor', 'r');
hold off;

